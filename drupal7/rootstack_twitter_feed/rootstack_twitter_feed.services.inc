<?php
/**
 * @file
 * rootstack_twitter_feed.services
 */

/**
 * Implements hook_services_resources().
 */
function rootstack_twitter_feed_services_resources() {
  return array(
    'rootstack_twitter_feed_posts_get' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'rootstack_twitter_feed',
          'name' => 'rootstack_twitter_feed.resources',
        ),
        'help' => t('Gets the home posts of rootstack twitter.'),
        'callback' => 'rootstack_twitter_feed_posts_get',
        'args' => array(),
        'access arguments' => array('rootstack_twitter_feed')
      ),
    ),
  );
}
