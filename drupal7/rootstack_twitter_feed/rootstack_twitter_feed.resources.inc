<?php
/**
 * @file
 * rootstack_twitter_feed.resources
 */

/**
 * Callback function to get rootstack twitter home posts.
 */
function rootstack_twitter_feed_posts_get() {
  global $conf;

  $url = $conf['twitter_url'];
  $oauth_access_token = $conf['twitter_oauth_access_token'];
  $oauth_access_token_secret = $conf['twitter_oauth_access_token_secret'];
  $consumer_key = $conf['twitter_consumer_key'];
  $consumer_secret = $conf['twitter_consumer_secret'];

  $oauth = array(
    'oauth_consumer_key' => $consumer_key,
    'oauth_nonce' => time(),
    'oauth_signature_method' => 'HMAC-SHA1',
    'oauth_token' => $oauth_access_token,
    'oauth_timestamp' => time(),
    'oauth_version' => '1.0');
  $base_info = rootstack_twitter_feed_build_base_string($url, 'GET', $oauth);
  $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
  $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
  $oauth['oauth_signature'] = $oauth_signature;
  $header = array(rootstack_twitter_feed_build_authorization_header($oauth), 'Expect:');
  $options = array( CURLOPT_HTTPHEADER => $header,
    CURLOPT_HEADER => false,
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false);

  $feed = curl_init();
  curl_setopt_array($feed, $options);
  $json = curl_exec($feed);
  curl_close($feed);
  $twitter_data = json_decode($json);
  $response = rootstack_twitter_feed_format_response($twitter_data);

  return $response;
}

/**
 * Function for response format
 *
 * @param $result
 *
 * @return array
 */
function rootstack_twitter_feed_format_response($result) {
  $response = [];

  foreach ($result as $tweet) {
    $regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
    $formated_text = preg_replace($regex, ' ', $tweet->text);
    $response[] = [
      'created_at' => $tweet->created_at,
      'text' => $formated_text,
      'user' => [
        'name' => $tweet->user->name,
        'tagname' => $tweet->user->screen_name,
        'url' => $tweet->user->url,
      ]
    ];

  }

  return $response;
}

/**
 * Build the string for the request
 *
 * @param $base_uri
 * @param $method
 * @param $params
 *
 * @return string
 */
function rootstack_twitter_feed_build_base_string($base_uri, $method, $params)
{
  $r = array();
  ksort($params);
  foreach($params as $key=>$value){
    $r[] = "$key=" . rawurlencode($value);
  }
  return $method."&" . rawurlencode($base_uri) . '&' . rawurlencode(implode('&', $r));
}

/**
 * Build the authorization header for OAuth
 *
 * @param $twitter_oauth_credentials
 *
 * @return string
 */
function rootstack_twitter_feed_build_authorization_header($twitter_oauth_credentials)
{
  $parsed_credentials = 'Authorization: OAuth ';
  $values = array();
  foreach($twitter_oauth_credentials as $key=>$value)
    $values[] = "$key=" . rawurlencode($value) . "";
  $parsed_credentials .= implode(', ', $values);
  return $parsed_credentials;
}
