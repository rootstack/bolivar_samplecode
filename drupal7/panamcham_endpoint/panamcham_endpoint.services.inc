<?php
/**
 * @file
 * rootstack_technologies.services.inc
 */

/**
 * Implements of hook_services_resources().
 */
function panamcham_endpoint_services_resources() {
  return array(
    'panamcham_get_event_list' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham event list'),
        'callback' => 'panamcham_get_event_list',
        'args' => array(
          array(
            'name' => 'language',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'language'),
            'description' => t('Language for the content.'),
          ),
          array(
            'name' => 'total',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'total'),
            'description' => t('Bring the whole list of technologies'),
          ),
          array(
            'name' => 'title',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'title'),
            'description' => t('Event title'),
          ),
          array(
            'name' => 'category',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'category'),
            'description' => t('Event category'),
          ),
          array(
            'name' => 'dateStart',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'dateStart'),
            'description' => t('Event Date lower range'),
          ),
          array(
            'name' => 'dateEnd',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'dateEnd'),
            'description' => t('Event Date upper range'),
          ),
          array(
            'name' => 'presenters',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'presenters'),
            'description' => t('Event presenters'),
          ),
          array(
            'name' => 'order',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'order'),
            'description' => t('Event order peer creation date'),
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_get_event' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham event'),
        'callback' => 'panamcham_get_event',
        'args' => array(
          array(
            'name' => 'language',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'language'),
            'description' => t('Language for the content.'),
          ),
          array(
            'name' => 'nid',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'nid'),
            'description' => t('Event node id'),
          ),
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_get_default_sponsors' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham sponsors'),
        'callback' => 'panamcham_get_default_sponsors',
        'args' => array(),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_get_home_slider' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham home slider'),
        'callback' => 'panamcham_get_home_slider',
        'args' => array(
          array(
            'name' => 'language',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array(),
            'description' => t('Language for the content.')
          ),
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_mark_as_read' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham mark as ready any content type'),
        'callback' => 'panamcham_mark_as_read',
        'args' => array(
          array(
            'name' => 'entity_id',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'entity_id'),
            'description' => t('Entity Id')
          ),
          array(
            'name' => 'entity_type',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'entity_type'),
            'description' => t('Relative to Node Content type'),
          ),
          array(
            'name' => 'user_id',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'user_id'),
            'description' => t('User Id'),
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_push_token' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham push device token'),
        'callback' => 'panamcham_push_token',
        'args' => array(
          array(
            'name' => 'token',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'token'),
            'description' => t('Push notification token')
          ),
          array(
            'name' => 'user_id',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'user_id'),
            'description' => t('User id')
          ),
          array(
            'name' => 'enabled',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'enabled'),
            'description' => t('Check if the user is logged')
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_endpoint_test_mail' => array(
      'create' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham test mail'),
        'callback' => 'panamcham_endpoint_test_mail',
        'args' => array(
          array(
            'name' => 'data',
            'optional' => FALSE,
            'source' => 'data',
            'type' => 'struct',
            'description' => 'Values'
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_get_count_unread' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham get count of unread content type'),
        'callback' => 'panamcham_get_count_unread',
        'args' => array(
          array(
            'name' => 'language',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'language'),
            'description' => t('Language for the content.')
          ),
          array(
            'name' => 'entity_type',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'entity_type'),
            'description' => t('Relative to Node Content type'),
          ),
          array(
            'name' => 'user_id',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'user_id'),
            'description' => t('User Id'),
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_get_news_list' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham news'),
        'callback' => 'panamcham_get_news_list',
        'args' => array(
          array(
            'name' => 'language',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'language'),
            'description' => t('Language for the content.')
          ),
          array(
            'name' => 'total',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'total'),
            'description' => t('Bring the whole list of technologies'),
          ),
          array(
            'name' => 'title',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'title'),
            'description' => t('Event title'),
          ),
          array(
            'name' => 'category',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'category'),
            'description' => t('Event category'),
          ),
          array(
            'name' => 'createStart',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'createStart'),
            'description' => t('Event creation lower range'),
          ),
          array(
            'name' => 'createEnd',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'createEnd'),
            'description' => t('Event creation date upper range'),
          ),
          array(
            'name' => 'order',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'order'),
            'description' => t('Event order peer creation date'),
          ),
          array(
            'name' => 'user_id',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'user_id'),
            'description' => t('User id'),
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_get_directory_list' => array(
      'index' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'help' => t('Panamcham Directory List'),
        'callback' => 'panamcham_get_directory_list',
        'args' => array(
          array(
            'name' => 'language',
            'type' => 'string',
            'optional' => FALSE,
            'source' => array('param' => 'language'),
            'description' => t('Language for the content.'),
          ),
          array(
            'name' => 'total',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'total'),
            'description' => t('Bring the whole list of technologies'),
          ),
          array(
            'name' => 'title',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'title'),
            'description' => t('Directory member title'),
          ),
          array(
            'name' => 'industry',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'industry'),
            'description' => t('Directory industry'),
          ),
          array(
            'name' => 'order',
            'type' => 'string',
            'optional' => TRUE,
            'source' => array('param' => 'order'),
            'description' => t('Event order peer creation date'),
          )
        ),
        'access arguments' => array('access content'),
      ),
    ),
    'panamcham_post_contact_form' => array(
      'create' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'access callback' => 'user_access',
        'access arguments' => array('access content'),
        'access arguments append' => FALSE,
        'help' => t('Panamcham Contact Submission service.'),
        'callback' => 'panamcham_post_contact_form',
        'args' => array(
          array(
            'name' => 'data',
            'optional' => FALSE,
            'source' => 'data',
            'type' => 'struct',
            'description' => 'Webform values'
          )
        ),
      ),
    ),
    'panamcham_post_user_register' => array(
      'create' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'access callback' => 'user_access',
        'access arguments' => array('access content'),
        'access arguments append' => FALSE,
        'help' => t('Panamcham user register service.'),
        'callback' => 'panamcham_post_user_register',
        'args' => array(
          array(
            'name' => 'data',
            'optional' => FALSE,
            'source' => 'data',
            'type' => 'struct',
            'description' => 'Webform values'
          )
        ),
      ),
    ),
    'panamcham_request_password' => array(
      'create' => array(
        'file' => array(
          'type' => 'inc',
          'module' => 'panamcham_endpoint',
          'name' => 'panamcham_endpoint.resources',
        ),
        'access callback' => 'user_access',
        'access arguments' => array('access content'),
        'access arguments append' => FALSE,
        'help' => t('Panamcham user recover password service.'),
        'callback' => 'panamcham_request_password',
        'args' => array(
          array(
            'name' => 'data',
            'type' => 'struct',
            'description' => 'A valid user name or e-mail address',
            'source' => 'data',
            'optional' => FALSE,
          )
        ),
      ),
    ),
  );
}

