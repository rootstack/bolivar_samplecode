<?php
/**
 * @file
 * panamcham_endpoint.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function panamcham_endpoint_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'member_directory_endpoint';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_amcham_index';
  $view->human_name = 'Member Directory Endpoint';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['css_class'] = 'col-lg-12';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['link_display'] = 'custom_url';
  $handler->display->display_options['link_url'] = 'directory';
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['exposed_form']['options']['expose_sort_order'] = FALSE;
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Indexed Contenido: Group Info */
  $handler->display->display_options['relationships']['field_member_group_info']['id'] = 'field_member_group_info';
  $handler->display->display_options['relationships']['field_member_group_info']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['relationships']['field_member_group_info']['field'] = 'field_member_group_info';
  /* Field: Indexed Contenido: ID del nodo */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['nid']['link_to_entity'] = 0;
  /* Field: Indexed Contenido: Logo */
  $handler->display->display_options['fields']['field_member_logo']['id'] = 'field_member_logo';
  $handler->display->display_options['fields']['field_member_logo']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_logo']['field'] = 'field_member_logo';
  $handler->display->display_options['fields']['field_member_logo']['label'] = '';
  $handler->display->display_options['fields']['field_member_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_member_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_member_logo']['settings'] = array(
    'image_style' => 'member_directory_logo',
    'image_link' => '',
  );
  /* Field: Indexed Contenido: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_entity'] = 0;
  /* Field: Indexed Contenido: Industries */
  $handler->display->display_options['fields']['field_member_industries']['id'] = 'field_member_industries';
  $handler->display->display_options['fields']['field_member_industries']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_industries']['field'] = 'field_member_industries';
  $handler->display->display_options['fields']['field_member_industries']['label'] = '';
  $handler->display->display_options['fields']['field_member_industries']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_member_industries']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_member_industries']['view_mode'] = 'full';
  $handler->display->display_options['fields']['field_member_industries']['bypass_access'] = 0;
  /* Field: Indexed Contenido: Mailing Address */
  $handler->display->display_options['fields']['field_member_mailing_address']['id'] = 'field_member_mailing_address';
  $handler->display->display_options['fields']['field_member_mailing_address']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_mailing_address']['field'] = 'field_member_mailing_address';
  $handler->display->display_options['fields']['field_member_mailing_address']['label'] = 'Dirección de Correos';
  /* Field: Indexed Contenido: Address */
  $handler->display->display_options['fields']['field_member_street_address']['id'] = 'field_member_street_address';
  $handler->display->display_options['fields']['field_member_street_address']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_street_address']['field'] = 'field_member_street_address';
  $handler->display->display_options['fields']['field_member_street_address']['label'] = 'Dirección';
  /* Field: Indexed Contenido: Website */
  $handler->display->display_options['fields']['field_member_web_site']['id'] = 'field_member_web_site';
  $handler->display->display_options['fields']['field_member_web_site']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_web_site']['field'] = 'field_member_web_site';
  $handler->display->display_options['fields']['field_member_web_site']['label'] = 'Sitio web';
  $handler->display->display_options['fields']['field_member_web_site']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_member_web_site']['alter']['path'] = '[field_member_web_site]';
  $handler->display->display_options['fields']['field_member_web_site']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_member_web_site']['alter']['external'] = TRUE;
  /* Field: Indexed Contenido: Office Phone */
  $handler->display->display_options['fields']['field_member_office_phone']['id'] = 'field_member_office_phone';
  $handler->display->display_options['fields']['field_member_office_phone']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_office_phone']['field'] = 'field_member_office_phone';
  $handler->display->display_options['fields']['field_member_office_phone']['label'] = 'Teléfono de oficina';
  /* Field: Indexed Contenido: Fax */
  $handler->display->display_options['fields']['field_member_fax']['id'] = 'field_member_fax';
  $handler->display->display_options['fields']['field_member_fax']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_fax']['field'] = 'field_member_fax';
  /* Field: Indexed Contenido: Group Info */
  $handler->display->display_options['fields']['field_member_group_info']['id'] = 'field_member_group_info';
  $handler->display->display_options['fields']['field_member_group_info']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_group_info']['field'] = 'field_member_group_info';
  $handler->display->display_options['fields']['field_member_group_info']['label'] = 'Representative';
  $handler->display->display_options['fields']['field_member_group_info']['hide_empty'] = TRUE;
  $handler->display->display_options['fields']['field_member_group_info']['list']['mode'] = 'list';
  $handler->display->display_options['fields']['field_member_group_info']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['field_member_group_info']['display'] = 'view';
  $handler->display->display_options['fields']['field_member_group_info']['view_mode'] = 'block_layout';
  $handler->display->display_options['fields']['field_member_group_info']['bypass_access'] = 0;
  /* Field: Indexed Contenido: Member from */
  $handler->display->display_options['fields']['field_member_from_date']['id'] = 'field_member_from_date';
  $handler->display->display_options['fields']['field_member_from_date']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_from_date']['field'] = 'field_member_from_date';
  $handler->display->display_options['fields']['field_member_from_date']['label'] = 'Member since';
  $handler->display->display_options['fields']['field_member_from_date']['settings'] = array(
    'format_type' => 'short',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Indexed Contenido: E-mail */
  $handler->display->display_options['fields']['field_member_email']['id'] = 'field_member_email';
  $handler->display->display_options['fields']['field_member_email']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['fields']['field_member_email']['field'] = 'field_member_email';
  $handler->display->display_options['fields']['field_member_email']['exclude'] = TRUE;
  /* Contextual filter: Búsqueda: More like this */
  $handler->display->display_options['arguments']['search_api_views_more_like_this']['id'] = 'search_api_views_more_like_this';
  $handler->display->display_options['arguments']['search_api_views_more_like_this']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['arguments']['search_api_views_more_like_this']['field'] = 'search_api_views_more_like_this';
  $handler->display->display_options['arguments']['search_api_views_more_like_this']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['search_api_views_more_like_this']['summary']['format'] = 'default_summary';
  /* Filter criterion: Indexed Contenido: Tipo de contenido */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'member' => 'member',
  );
  /* Filter criterion: Indexed Contenido: Title */
  $handler->display->display_options['filters']['title']['id'] = 'title';
  $handler->display->display_options['filters']['title']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['filters']['title']['field'] = 'title';
  $handler->display->display_options['filters']['title']['exposed'] = TRUE;
  $handler->display->display_options['filters']['title']['expose']['operator_id'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['operator'] = 'title_op';
  $handler->display->display_options['filters']['title']['expose']['identifier'] = 'title';
  $handler->display->display_options['filters']['title']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    11 => 0,
    16 => 0,
  );
  /* Filter criterion: Indexed Contenido: Item language */
  $handler->display->display_options['filters']['search_api_language']['id'] = 'search_api_language';
  $handler->display->display_options['filters']['search_api_language']['table'] = 'search_api_index_amcham_index';
  $handler->display->display_options['filters']['search_api_language']['field'] = 'search_api_language';
  $handler->display->display_options['filters']['search_api_language']['value'] = array(
    'current' => 'current',
  );
  $export['member_directory_endpoint'] = $view;

  $view = new view();
  $view->name = 'member_industries';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Member Industries';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'News category endpoint';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Taxonomy term: Nombre */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = 'tid';
  $handler->display->display_options['fields']['tid']['element_default_classes'] = FALSE;
  /* Field: Taxonomy term: Language */
  $handler->display->display_options['fields']['language']['id'] = 'language';
  $handler->display->display_options['fields']['language']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['language']['field'] = 'language';
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'news_categories' => 'news_categories',
  );

  /* Display: Member Insdustries Endpoint */
  $handler = $view->new_display('services', 'Member Insdustries Endpoint', 'services_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Member Insdustries Endpoint';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Taxonomy term: Nombre */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['element_default_classes'] = FALSE;
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['label'] = 'tid';
  $handler->display->display_options['fields']['tid']['element_default_classes'] = FALSE;
  /* Field: Taxonomy term: Language */
  $handler->display->display_options['fields']['language']['id'] = 'language';
  $handler->display->display_options['fields']['language']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['language']['field'] = 'language';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Taxonomy term: Nombre */
  $handler->display->display_options['sorts']['name']['id'] = 'name';
  $handler->display->display_options['sorts']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['sorts']['name']['field'] = 'name';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'member_industries' => 'member_industries',
  );
  $handler->display->display_options['path'] = 'panamcham_get_industries_categories';

  /* Display: News category endpoint */
  $handler = $view->new_display('services', 'News category endpoint', 'services_2');
  $handler->display->display_options['path'] = 'panamcham_get_news_categories';

  /* Display: Event type endpoint */
  $handler = $view->new_display('services', 'Event type endpoint', 'services_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Event type endpoint';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'event_type' => 'event_type',
  );
  $handler->display->display_options['path'] = 'panamcham_get_event_categories';
  $export['member_industries'] = $view;

  $view = new view();
  $view->name = 'presenters_endpoint';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Presenters endpoint';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Presenters Endpoint';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Contenido: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = 'nid';
  $handler->display->display_options['fields']['nid']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['nid']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['nid']['node_in_colorbox_rel'] = '';
  /* Field: Contenido: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  $handler->display->display_options['fields']['title']['node_in_colorbox_width'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_height'] = '600';
  $handler->display->display_options['fields']['title']['node_in_colorbox_rel'] = '';
  /* Field: Contenido: Language */
  $handler->display->display_options['fields']['language']['id'] = 'language';
  $handler->display->display_options['fields']['language']['table'] = 'node';
  $handler->display->display_options['fields']['language']['field'] = 'language';
  $handler->display->display_options['fields']['language']['label'] = 'language';
  /* Field: Contenido: Company */
  $handler->display->display_options['fields']['field_people_company']['id'] = 'field_people_company';
  $handler->display->display_options['fields']['field_people_company']['table'] = 'field_data_field_people_company';
  $handler->display->display_options['fields']['field_people_company']['field'] = 'field_people_company';
  $handler->display->display_options['fields']['field_people_company']['label'] = 'company';
  $handler->display->display_options['fields']['field_people_company']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_people_company']['type'] = 'text_plain';
  /* Field: Contenido: Position */
  $handler->display->display_options['fields']['field_people_company_position']['id'] = 'field_people_company_position';
  $handler->display->display_options['fields']['field_people_company_position']['table'] = 'field_data_field_people_company_position';
  $handler->display->display_options['fields']['field_people_company_position']['field'] = 'field_people_company_position';
  $handler->display->display_options['fields']['field_people_company_position']['label'] = 'position';
  $handler->display->display_options['fields']['field_people_company_position']['type'] = 'text_plain';
  /* Field: Contenido: Image */
  $handler->display->display_options['fields']['field_people_profile_image']['id'] = 'field_people_profile_image';
  $handler->display->display_options['fields']['field_people_profile_image']['table'] = 'field_data_field_people_profile_image';
  $handler->display->display_options['fields']['field_people_profile_image']['field'] = 'field_people_profile_image';
  $handler->display->display_options['fields']['field_people_profile_image']['label'] = 'image';
  $handler->display->display_options['fields']['field_people_profile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_people_profile_image']['type'] = 'image_url';
  $handler->display->display_options['fields']['field_people_profile_image']['settings'] = array(
    'url_type' => '0',
    'image_style' => '',
    'image_link' => '',
  );
  /* Sort criterion: Contenido: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Contenido: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Contenido: Tipo */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'people' => 'people',
  );
  /* Filter criterion: Contenido: Language */
  $handler->display->display_options['filters']['language']['id'] = 'language';
  $handler->display->display_options['filters']['language']['table'] = 'node';
  $handler->display->display_options['filters']['language']['field'] = 'language';
  $handler->display->display_options['filters']['language']['value'] = array(
    '***CURRENT_LANGUAGE***' => '***CURRENT_LANGUAGE***',
  );

  /* Display: Presenters Endpoint */
  $handler = $view->new_display('services', 'Presenters Endpoint', 'services_1');
  $handler->display->display_options['path'] = 'panamcham_get_presenters';
  $export['presenters_endpoint'] = $view;

  return $export;
}
