<?php
/**
 * @file
 * panamcham_endpoint.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function panamcham_endpoint_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'member_redirect';
  $page->task = 'page';
  $page->admin_title = 'Member redirect';
  $page->admin_description = '';
  $page->path = '/role/member/updated';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_member_redirect__panel';
  $handler->task = 'page';
  $handler->subtask = 'member_redirect';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Role Member revert',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Language',
        'keyword' => 'language',
        'name' => 'language',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'plugins' => array(),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd689713e-5e2a-4c1f-8c98-1951380925d2';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_member_redirect__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-bf9079d9-fc1e-4689-b1db-0517350360ac';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'es' => 'es',
            'default' => 0,
            'en' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => 'Member role update success',
    'title' => 'Felicidades',
    'title_heading' => 'h2',
    'body' => 'Usted ha restaurado su contraseña.',
    'format' => 'filtered_html',
    'substitute' => TRUE,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'bf9079d9-fc1e-4689-b1db-0517350360ac';
  $display->content['new-bf9079d9-fc1e-4689-b1db-0517350360ac'] = $pane;
  $display->panels['middle'][0] = 'new-bf9079d9-fc1e-4689-b1db-0517350360ac';
  $pane = new stdClass();
  $pane->pid = 'new-907aa2b3-ad42-4a86-ba8b-4b44f73e194e';
  $pane->panel = 'middle';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'site_language',
        'settings' => array(
          'language' => array(
            'en' => 'en',
            'default' => 0,
            'es' => 0,
          ),
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'admin_title' => '',
    'title' => 'Congratulations',
    'title_heading' => 'h2',
    'body' => '<p>You have reseted your password</p>
',
    'format' => 'filtered_html',
    'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '907aa2b3-ad42-4a86-ba8b-4b44f73e194e';
  $display->content['new-907aa2b3-ad42-4a86-ba8b-4b44f73e194e'] = $pane;
  $display->panels['middle'][1] = 'new-907aa2b3-ad42-4a86-ba8b-4b44f73e194e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['member_redirect'] = $page;

  return $pages;

}
