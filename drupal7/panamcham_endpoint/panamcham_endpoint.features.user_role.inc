<?php
/**
 * @file
 * panamcham_endpoint.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function panamcham_endpoint_user_default_roles() {
  $roles = array();

  // Exported role: member.
  $roles['member'] = array(
    'name' => 'member',
    'weight' => 4,
  );

  return $roles;
}
