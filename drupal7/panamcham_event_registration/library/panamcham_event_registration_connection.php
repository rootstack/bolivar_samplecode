<?php


/**
 * Get the configuration of the event.
 *
 * @return bool|mixed|string
 */
function get_crm_contact_subscribe_config() {
  global $conf;

  $ch = curl_init();
  $event_url = $conf['rootnet_host'] . 'subscribe/configuration?apikey=' . $conf['apikey'] . '&client_name=' . $conf['client_name'];
  curl_setopt($ch, CURLOPT_URL, $event_url);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_NOBODY, FALSE); // remove body
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  $response = curl_exec($ch);
  $response = json_decode($response);
  curl_close($ch);

  return $response;
}

/**
 * Subscribe a contact in rootnet.
 *
 * @param $data
 *
 * @return bool|mixed|string
 */
function add_contact($data) {
  global $conf;

  $body = [
    'apikey' => $conf['apikey'],
    'client_name' => $conf['client_name'],
    'contact' => [
      'First Name' => $data['First_Name'],
      'Last Name' => $data['Last_Name'],
      'Email' => $data['Email'],
    ],
  ];
  $ch = curl_init();
  $event_url = $conf['rootnet_host'] . 'subscribe/contact/add';
  curl_setopt($ch, CURLOPT_URL, $event_url);
  curl_setopt($ch, CURLOPT_HEADER, FALSE);
  curl_setopt($ch, CURLOPT_NOBODY, FALSE); // remove body
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
  $response = curl_exec($ch);
  $response = json_decode($response);
  curl_close($ch);

  return $response;
}

/**
 * Get the saved invitations from database.
 *
 * @param $build_id
 *
 * @return array
 */
function getInvitations($build_id) {
  $invitations = [];

  try {
    $invitations = db_select('event_invitation', 'e')
      ->fields('e', [
        'first_name',
        'last_name',
        'email',
        'form_build_id',
        'company',
        'member',
      ])
      ->condition('e.form_build_id', $build_id)
      ->execute()
      ->fetchAll();

    return $invitations;

  } catch (Exception $e) {
    watchdog('panamcham_event_registration','Error occurred retrieving the invitations');
  }

  return $invitations;
}

/**
 * Check if the invitation exist.
 *
 * @param $form_state
 * @param $build_id
 *
 * @return array
 */
function getInvitationByEmail($form_state, $build_id) {
  $invitations = [];

  try {
    $invitations = db_select('event_invitation', 'e')
      ->fields('e', [
        'first_name',
        'last_name',
        'email',
        'form_build_id',
        'company',
        'member',
      ])
      ->condition('e.form_build_id', $build_id)
      ->condition('e.email', $form_state['values']['Email'])
      ->execute()
      ->fetchAll();

    return $invitations;

  } catch (Exception $e) {
    watchdog('panamcham_event_registration','Error occurred retrieving the invitations by email');
  }

  return $invitations;
}

/**
 * Insert the invitation in database.
 *
 * @param $form_state
 * @param $build_id
 * @param bool $invitation
 */
function setInvitations($form_state, $build_id, $invitation = FALSE) {
  try {
    $member = FALSE;
    if ($invitation) {
      $member = $form_state['values']['invitation_member'] ? $form_state['values']['invitation_member'] :
        0;
    }
    db_insert('event_invitation')
      ->fields(
        [
          'first_name' => $form_state['values']['First_Name'],
          'last_name' => $form_state['values']['Last_Name'],
          'email' => $form_state['values']['Email'],
          'form_build_id' => $build_id,
          'company' => $form_state['values']['invitation_member_value'],
          'member' => $member,
        ]
      )->execute();
  } catch (Exception $e) {
    watchdog('panamcham_event_registration','Error occurred inserting the invitations');
  }
}
