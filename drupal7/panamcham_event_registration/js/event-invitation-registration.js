/**
 * @file
 * Custom scripts for theme.
 */
(function ($, Drupal) {
    let savedInvitations = Drupal.settings.saved_invitations;

    Drupal.behaviors.panamcham_event_invitation_registration = {
        savedInvitations,
        attach: function (context, settings) {
            let invitations = Drupal.settings.event_registration_invitation_elements;
            let action = Drupal.settings.operation;
            let formData = Drupal.settings.event_registration_invitation_data;
            let invitationExist = Drupal.settings.element_exist;
            if(action == 'add') {
                if (!savedInvitations) {
                    savedInvitations = 0;
                }
                Drupal.settings.savedInvitations = savedInvitations;
                let data = Drupal.settings.event_registration_invitation_data;
                if (typeof data === undefined) {
                    alert('Ocurrio un error al agregar el Invitado');
                } else {
                    if (invitations) {
                        if (!invitationExist) {
                            savedInvitations ++;
                            $('.event-invitation-number').text(savedInvitations);
                            alert('Invitado ' + data[0].First_Name + ' Agregado exitosamente');
                            let formKeys = Object.keys(data[0]);
                            $.each(formKeys, function(index, key) {
                                if (key !== 'save' || key !== 'op') {
                                    $('#panamcham-event-registration-invitation-form .form-item input[name='+ key +']').val('');
                                }
                            })
                        } else {
                            alert('El invitado ' + formData[0].First_Name + ' Con correo ' +formData[0].Email + ' ya existe.');
                        }
                    }
                }
                if (savedInvitations == Drupal.settings.invitationsQuantity) {
                    $('#panamcham-event-registration-form .form-submit').css({display: 'block'});
                    $('#panamcham-event-registration-invitation-form  #edit-save').css({display: 'none'});
                }
            }
        }
    }

}(jQuery, Drupal));
