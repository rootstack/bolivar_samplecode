/**
 * @file
 * Custom scripts for theme.
 */
(function ($, Drupal) {
    Drupal.behaviors.panamcham_event_registration = {
        attach: function (context, settings) {
        let action = Drupal.settings.operation;
            if(action == 'register') {
                let eventRegistrationForm = Drupal.settings.event_registration_data;
                let eventRegistrationElements = Drupal.settings.event_registration_invitation_elements;
                let sendInvitations =  Drupal.settings.send_invitations;
                let client_name = Drupal.settings.client_name;
                const apiKey = Drupal.settings.apikey;
                const host = Drupal.settings.rootnet_host;
                if (eventRegistrationForm) {
                    getUser(eventRegistrationForm);
                    if ($('.event-invitation-check').val() !== false) {
                        if (typeof eventRegistrationElements !== undefined && sendInvitations === 'true') {
                            $.each(eventRegistrationElements, function (index, value) {
                                getUser(value, true);
                            });
                        }
                    }

                    $('.event-registration-form .event-registration-success').css({display: 'block'});

                    setTimeout(location.reload(), 2000);

                }

                function getUser(eventRegistrationForm, invited = false) {
                    let body = {
                        contact: {},
                        apikey: apiKey,
                        client_name: client_name,
                    };

                    const member_info = {
                        company_value: invited ? eventRegistrationForm['company'] :
                            eventRegistrationForm['member_value'],
                        member: invited ? parseInt(eventRegistrationForm['member']) :
                            eventRegistrationForm['member'],
                    };

                    body.contact['First Name'] = eventRegistrationForm['first_name'];
                    body.contact['Last Name'] = eventRegistrationForm['last_name'];
                    body.contact['Email'] = eventRegistrationForm['email'];

                    if (member_info.member) {
                        body['account_id'] = member_info.company_value;
                    }

                    $.ajax
                    ({
                        type: "POST",
                        //the url where you want to sent the userName and password to
                        url: host + 'subscribe/contact/add',
                        contentType : 'application/json',
                        async: false,
                        //json object to sent to the authentication url
                        data: JSON.stringify(body),
                        success: function (response) {
                            if (response.contact) {
                                if (!invited) {
                                    Drupal.settings.event_registration_owner = response.contact;
                                    subscribeUserEvent(response.contact, invited, member_info);
                                } else {
                                    subscribeUserEvent(response.contact, invited, member_info);
                                }
                            }
                        }
                    })
                }

                function subscribeUserEvent(user, invited, member_info) {
                    const url = host+"event_registration";
                    let body = {
                        contacts: [user],
                        event_id: Drupal.settings.eventId,
                        event_registration_type_id: Drupal.settings.event_registration_type_id,
                        apikey: apiKey,
                        client_name: client_name
                    };

                    body.contacts[0].metas = [];

                    let parsedBody = JSON.stringify(body);

                    let eventRegistrationConfig = getEventRegistrationConfig(user);

                    if (invited) {
                        let registrationOwner = Drupal.settings.event_registration_owner;
                        if (eventRegistrationConfig.event_registration_type) {
                            $.each(eventRegistrationConfig.event_registration_type.fields, function (index, field) {
                                if (field.field_name === 'Invitado') {
                                    let contact_info = {
                                        id: registrationOwner.id,
                                        full_name: registrationOwner.full_name,
                                        status: registrationOwner.active,
                                        email: registrationOwner.email
                                    };
                                    body.contacts[0].metas.push({
                                        event_registration_type_field_id: field.id,
                                        value: JSON.stringify(contact_info),
                                    });
                                }
                                if (field.field_name === 'Empresa (No Miembro)' && !member_info.member) {
                                    body.contacts[0].metas.push({
                                        event_registration_type_field_id: field.id,
                                        value: JSON.stringify(member_info.company_value),
                                    });
                                }
                            });
                        }

                        parsedBody = JSON.stringify(body);
                    } else {
                        if (eventRegistrationConfig.event_registration_type) {
                            $.each(eventRegistrationConfig.event_registration_type.fields, function (index, field) {
                                if (field.field_name === 'Empresa (No Miembro)' && !member_info.member) {
                                    body.contacts[0].metas.push({
                                        event_registration_type_field_id: field.id,
                                        value: JSON.stringify(member_info.company_value),
                                    });
                                }
                            });
                        }

                        parsedBody = JSON.stringify(body);
                    }

                    $.ajax
                    ({
                        type: "POST",
                        //the url where you want to sent the userName and password to
                        url: url,
                        contentType : 'application/json',
                        async: false,
                        //json object to sent to the authentication url
                        data: parsedBody,
                        success: function (response) {
                            if (response && !invited) {
                            }
                        }
                    });
                }

                function getEventRegistrationConfig(user) {
                    let event_id = Drupal.settings.event_registration_type_id;
                    const url = host + 'event_registration/types/'+event_id+'?apikey='+apiKey+'&client_name='+client_name;
                    let config = null;
                    $.ajax
                    ({
                        type: "GET",
                        url: url,
                        contentType : 'application/json',
                        async: false,
                        success: function (response) {
                            if (response.event_registration_type) {
                                config = response;
                            }
                        }
                    });

                    return config;
                }
            }
        },
        detach: function(context) {
        }
    };
}(jQuery, Drupal));
