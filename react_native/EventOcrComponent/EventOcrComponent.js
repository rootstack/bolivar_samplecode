import React, { Component, Fragment } from 'react';
import {
  View, Text, TouchableOpacity, ScrollView,
  Alert, Image, KeyboardAvoidingView, Dimensions
} from 'react-native';
import _ from 'lodash';
import { Permissions, ImageManipulator } from 'expo';
import { Camera } from 'expo-camera';
import { connect } from 'react-redux';
import { styles } from './EventOcrComponent.style';
import Header from '../commons/Header';
import Loading from '../commons/Loading';
import AppInput from '../commons/Input';
import Button from '../commons/Button';
import {
  postSubscribeContact,
  getContactSubscribeConfig,
  getContactTypeFields
} from '../../actions/userAction';
import { sendScannedImage, getOcrConfig } from '../../actions/ocrAction';
import {
  getEventRegistration,
  clearEventData,
  postEventRegistration,
  patchEventRegistration
} from '../../actions/eventAction';
import { globalStyles } from '../globalStyles/style';
import { checkRedirection } from '../util/actionRedirection';
import { loadAppSettings } from '../../actions/settingsAction';
import EventOcrSteps from './EventOcrSteps';

class EventOcrComponent extends Component {
  constructor(props) {
    super(props);

    this.contactFound = false;
    this.initialState = {
      manual: false,
      cameraPermission: true,
      cameraType: Camera.Constants.Type.back,
      scanned: false,
      registeringContact: false,
      first_name: '',
      last_name: '',
      email: '',
      hasPermission: true,
      imageCaptured: false,
      scanningImage: false,
      imageProcessed: false,
      registeredContact: false,
      takedPicture: null,
      ocrResponse: null,
      showCancel: false,
      loading: false,
      canceled: false,
      showCamera: 'flex',
    };
    this.state = this.initialState;
  }

  componentDidMount() {
    this.askPermissions();
  }

  componentDidUpdate() {
    const { navigation } = this.state;

    if (!this.state.hasPermission) {
      navigation.navigate('home');
    }
  }

  resetVariables() {
    clearEventData();
    setTimeout(() => {
      clearEventData();
      this.setState({ ...this.initialState });
    }, 3000);
  }

  onInputPress() {
    this.setState({
      scanned: false,
      captureImage: false,
      manual: true,
      ocrResponse: {}
    });
  }

  async onContinuePress() {
    const params = {};
    const {
      ocrResponse,
      contactSubscribeConfig
    } = this.state;
    const { navigation, screenProps } = this.props,
      { translate } = screenProps;
    const selectedEvent = navigation.getParam('selectedEvent'),
      { event_registration_type } = selectedEvent;
    const selectedCheckFields = navigation.getParam('eventSelectedCheckField');
    const contactCreate = navigation.getParam('eventContactCreate', false);

    params.event_id = selectedEvent.id;
    params.ocrResponse = ocrResponse;
    params.event_registration_type_id = event_registration_type.id;
    params.event_registration_metas = selectedCheckFields;
    params.contactSubscribeConfig = contactSubscribeConfig;

    this.setState({ registeringContact: true });
    if (contactCreate) {
      const subscribeContact = await postSubscribeContact(params, selectedCheckFields);
      const { eventRegistrationBody } = subscribeContact;
      const registerContact = await postEventRegistration(eventRegistrationBody, selectedCheckFields);

      if (!registerContact) {
        Alert.alert('Error', translate.t('registrationError'));
      } else {
        this.setState({ registeredContact: true });
        this.resetVariables();
      }
    } else {
      const eventRegistration = await getEventRegistration(params);

      if (eventRegistration.length < 1) {
        Alert.alert('Error', translate.t('notEventRegistered'));
      } else {
        const eventRegistrationParams = {
          event_registration_id: params.event_id,
          eventContact: eventRegistration,
          event_registration_metas: params.event_registration_metas,
        };

        const updateSuccess = await patchEventRegistration(eventRegistrationParams);

        if (!updateSuccess) {
          Alert.alert('Error', translate.t('markingAssistanceError'));
        } else {
          this.setState({ registeredContact: true });
          this.resetVariables();
        }
      }
    }
  }

  onCancelPress() {
    clearEventData();
    this.setState({
      ...this.initialState,
      canceled: true
    });
  }

  async askPermissions() {
    const { clientConfig, navigation } = this.props,
      { client } = clientConfig.clientConfig;
    const { status } = await Permissions.getAsync(Permissions.CAMERA);
    const hasPermission = 'ocr_crm_manage' in client.new_permissions;

    if (hasPermission && status) {
      this.setState({ loading: true });
      const ocrConfig = await getOcrConfig();
      const contactConfig = await getContactTypeFields();
      const contactSubscribeConfig = await getContactSubscribeConfig(
        ocrConfig,
        contactConfig,
      );

      this.setState({
        loading: false,
        cameraPermission: status,
        contactSubscribeConfig
      });
    } else {
      Alert.alert(
        'WARNING',
        'YOU DO NOT HAVE PERMISSIONS',
        [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate('Home');
            }
          },
        ]
      );
    }
  }

  async takePicture() {
    const { ocr } = this.props,
      { ocrConfig } = ocr,
      { fields } = ocrConfig;
    const options = {
      format: 'png',
      quality: 0,
      base64: true,
    };
    const actions = [{ rotate: 90 }];
    const saveOptions = {
      format: 'png',
      base64: true
    };
    let newPicture = null;

    this.setState({
      ...this.initialState,
      scanned: true,
      captureImage: true,
      showCancel: true,
      manual: false,
    });

    const photo = await this.camera.takePictureAsync(options);
    const { width } = Dimensions.get('window');
    const { height } = Dimensions.get('window');

    if (width > height) {
      newPicture =
        await ImageManipulator.manipulateAsync(photo.uri, actions, saveOptions);
    } else {
      newPicture = photo.base64;
    }

    this.setState({
      takedPicture: photo,
      scanningImage: true,
      showCamera: 'none',
    });

    const ocrResponse = await sendScannedImage(newPicture.base64, fields);

    if (!this.state.canceled) {
      this.setState({
        imageProcessed: true,
        ocrResponse,
        showCancel: false,
      });
    }
  }

  onInfoChange(key = {}, value) {
    this.state.ocrResponse[key] = value;
    this.setState({ ...this.state });
  }

  async onReturnPress() {
    const { navigation, clientConfig } = this.props,
      { client } = clientConfig.clientConfig;
    const updatedSettings = await loadAppSettings(client.id);
    const checkHome = _.find(updatedSettings, { home: true });

    this.resetVariables();
    if (checkHome) {
      const { machineName, entity } = checkHome;

      checkRedirection(entity, navigation, machineName);
    } else {
      navigation.navigate('EventActionView');
    }
  }

  render() {
    const { screenProps, navigation, ocr } = this.props,
      { translate } = screenProps,
      event = navigation.getParam('selectedEvent');
    const contactCreate = navigation.getParam('eventContactCreate', false);

    return (
      <View style={{ flex: 1 }} collapsable={false}>
        {this.state.loading &&
          < Loading />
        }
        {!this.state.loading &&
          <Fragment>
            <Header
              navigation={navigation}
              translate={translate} />
            <View style={globalStyles.appContainer}>
              {!contactCreate &&
                <Text style={styles.eventTtitleStyle}>
                  {translate.t('markAsssistanceLabel')}</Text>
              }
              {contactCreate &&
                <Text style={{ fontSize: 24, fontFamily: 'roboto', marginTop: 20, textAlign: 'center' }}>
                  {translate.t('createContactLabel')}</Text>
              }
              <Text style={{ fontSize: 24, fontFamily: 'roboto', marginTop: 20, textAlign: 'center' }}>
                {event.title}</Text>
              {this.state.cameraPermission &&
                <View style={{ flex: 1 }}>
                  <ScrollView style={{ width: '100%', height: '100%', marginBottom: 50 }}>
                    {/* {!this.state.takedPicture && */}
                    <Camera style={{
                      display: this.state.showCamera,
                      marginTop: 20,
                      width: '100%',
                      height: 300,
                    }}

                      type={this.state.cameraType}
                      ref={(ref) => {
                        this.camera = ref;
                      }}>
                    </Camera>
                    {/* } */}
                    {this.state.takedPicture &&
                      <View style={{
                        marginTop: 20,
                        width: '100%',
                        height: 300,
                      }}>
                        <Image style={{ width: '100%', height: '100%' }} source={{ uri: this.state.takedPicture.uri }} />
                      </View>
                    }
                    <View style={{ flexDirection: 'row' }}>
                      <View style={{ width: '50%', height: '100%', alignItems: 'center' }}>
                        {!this.state.scanned &&
                          <View style={{ alignItems: 'center' }}>
                            <Text style={styles.scanTitleLabel}>
                              {translate.t('scanYourIdLabel')}
                            </Text>
                            <TouchableOpacity style={{ width: 100, height: 40, backgroundColor: 'green', marginTop: 30, marginBottom: 10, borderRadius: 3, justifyContent: 'center', alignItems: 'center' }}
                              onPress={this.takePicture.bind(this)}>
                              <Text style={styles.scanLabelStyle}>
                                {translate.t('scanLabel')}</Text>
                            </TouchableOpacity>
                            <Text style={styles.scanTitleLabel}>{translate.t('orLabel')}</Text>
                            < Text style={styles.scanTitleLabel} >{translate.t('typeManually')}</Text>
                            <TouchableOpacity style={{
                              width: 100, height: 40, backgroundColor: 'green', textAlign: 'center',
                              marginTop: 30, marginBottom: 10, borderRadius: 3, justifyContent: 'center', alignItems: 'center'
                            }}
                              onPress={this.onInputPress.bind(this)}>
                              <Text style={styles.scanLabelStyle}>
                                {translate.t('inputLabel')}</Text>
                            </TouchableOpacity>
                          </View>
                        }
                        <EventOcrSteps
                          state={this.state}
                          styles={styles}
                          onCancelPress={this.onCancelPress.bind(this)}
                          translate={translate} />
                        {this.state.imageProcessed &&
                          <TouchableOpacity style={{
                            width: 100, height: 40, backgroundColor: 'green', textAlign: 'center',
                            marginTop: 30, marginBottom: 10, borderRadius: 3, justifyContent: 'center', alignItems: 'center'
                          }}
                            onPress={this.takePicture.bind(this)}>
                            <Text style={styles.scanLabelStyle}>
                              {translate.t('tryAgainLabel')}</Text>
                          </TouchableOpacity>
                        }
                      </View>
                      <View style={{ width: '50%', height: '100%', marginTop: 20 }}>
                        {this.state.ocrResponse &&
                          <KeyboardAvoidingView
                            behavior="padding"
                            enabled
                            style={{ width: '100%' }}>
                            {this.state.scanned &&
                              <Fragment>
                                <Text style={{ fontWeight: 'bold', fontFamily: 'roboto', fontSize: 18, marginBottom: 20, textAlign: 'center', width: '100%' }}>{translate.t('detectedData')}</Text>
                                <Text style={{ fontWeight: 'bold', fontFamily: 'roboto', fontSize: 18, marginBottom: 20, textAlign: 'center', width: '100%' }}>{translate.t('verifyInformation')}</Text>
                              </Fragment>
                            }
                            {this.state.manual &&
                              <Text style={{ fontWeight: 'bold', fontFamily: 'roboto', fontSize: 18, marginBottom: 20, textAlign: 'center', width: '100%' }}>{translate.t('inputInformation')}</Text>
                            }
                            <View style={{ alignItems: 'center', justifyContent: 'center', width: '100%' }}>
                              {this.state.contactSubscribeConfig &&
                                (this.state.manual || this.state.ocrResponse) &&
                                this.state.contactSubscribeConfig.map((element, index) => {
                                  return (
                                    <View style={{
                                      width: '100%',
                                      marginBottom: 10,
                                      flexDirection: 'row'
                                    }} key={index}>
                                      <View style={{ width: '28%' }}>
                                        <Text style={styles.fieldLabelStyle}>{element.label}</Text>
                                      </View>
                                      <View style={{ width: '72%' }}>
                                        <AppInput
                                          secureTextEntry={false}
                                          autoCapitalize="none"
                                          value={this.state.ocrResponse[element.machineName]}
                                          inputStyle={{ width: '50%' }}
                                          onChangeText={this.onInfoChange.bind(this, element.machineName)}
                                        />
                                      </View>
                                    </View>
                                  );
                                })}
                            </View>
                            <View style={{ width: '100%', alignItems: 'center' }}>
                              <Button
                                title={translate.t('continueLabel')}
                                onPress={this.onContinuePress.bind(this)}
                              />
                              <Button
                                title={translate.t('cancelLabel')}
                                onPress={this.onCancelPress.bind(this)}
                              />
                            </View>
                          </KeyboardAvoidingView>
                        }
                      </View>
                    </View>
                  </ScrollView>
                  <View style={{ flex: 1, marginBottom: 10, justifyContent: 'space-between', alignItems: 'flex-end', flexDirection: 'row', }}>
                    <TouchableOpacity style={globalStyles.homeButtonStyle}
                      onPress={this.onReturnPress.bind(this)}>
                      <Text
                        style={globalStyles.backConfigTextStyle}>
                        {translate.t('backLabel')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>

              }
              {!this.state.cameraPermission &&
                <Text>NO CAMERA PERMISSION</Text>
              }
            </View>
          </Fragment>
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  const { domainData, login, clientConfig, ocr, event, user } = state;

  return {
    domainData,
    login,
    clientConfig,
    ocr,
    event,
    user
  };
};

export default connect(mapStateToProps)(EventOcrComponent);
