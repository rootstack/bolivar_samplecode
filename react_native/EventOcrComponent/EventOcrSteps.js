import React, { Fragment } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';

const EventOcrSteps = ({ state, styles, onCancelPress, translate }) => {
  return (
    <Fragment>
      {(state.scanned || state.manual) &&
        <Fragment>
          <View style={{ marginTop: 20 }}>
            {state.captureImage &&
              <Text style={styles.stepsTextStyle}>
                {translate.t('scanningImage')}
              </Text>
            }
            {state.imageCaptured &&
              <Text style={styles.stepsTextStyle}>
                {translate.t('imageCaptured')}
              </Text>
            }
            {state.scanningImage &&
              <Text style={styles.stepsTextStyle}>
                {translate.t('processingImage')}
              </Text>
            }
            {state.imageProcessed &&
              <Text style={styles.stepsTextStyle}>
                {translate.t('imageProcessed')}</Text>
            }
            {state.registeringContact &&
              <Text style={styles.stepsTextStyle}>
                {translate.t('registeringContact')}</Text>
            }
            {state.registeredContact &&
              <Text style={styles.stepsTextStyle}>
                {translate.t('reloadingIn')}
              </Text>
            }
          </View>
          {!state.manual && !state.imageProcessed &&
            < Button
              title={translate.t('cancelLabel')}
              onPress={onCancelPress}
            />
          }
        </Fragment>
      }
    </Fragment>
  );
};

export default EventOcrSteps;
