import React from 'react';
import { StyleSheet } from 'react-native';
import {
  heightPercentageToDP as hp, widthPercentageToDP as wp
} from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
  eventTtitleStyle: {
    fontSize: 24,
    fontFamily: 'roboto',
    marginTop: 20,
    textAlign: 'center',
  },
  scanTitleLabel: {
    alignSelf: 'center',
    fontFamily: 'roboto',
    textAlign: 'center',
    marginTop: 30,
    fontSize: wp('3%'),
  },
  scanLabelStyle: {
    fontWeight: 'bold',
    fontFamily: 'roboto',
    color: '#fff',
    fontSize: wp('2%'),
  },
  stepsTextStyle: {
    fontSize: wp('2%'),
    fontFamily: 'roboto',
    marginBottom: 10,
  },
  fieldLabelStyle: {
    fontSize: wp('2%'),
    fontFamily: 'roboto',
    marginBottom: 10
  }
});
