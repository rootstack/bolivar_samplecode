jQuery(document).ready(function() {

  jQuery(document).on('click', '.catalogo-open-video-modal', function(e) {
    jQuery('#colorbox').addClass('modal--material-editorial');
    jQuery('#colorbox #cboxLoadedContent .catalogo--video').show();
  });

  jQuery(document).on('click', '#cboxClose', function(e) {
    jQuery('#colorbox').removeClass('modal--material-editorial');
    jQuery('#colorbox #cboxLoadedContent .catalogo--video').hide();
  });

});