jQuery(document).ready(function(){
  jQuery('.navbar-list-item ul').addClass('collapse-dropdown');

  // jQuery('.sub-main-navigation-link ').hover(function () {
  //   jQuery(this).parent().addClass('active');
  //   jQuery(this).parent().mouseleave(function() {
  //     jQuery('.navbar-list-item .active').removeClass('active');
  //   });
  // });
  //
  // jQuery('.main-navigation-link').hover(function () {
  //   jQuery(this).next().css({'display': 'block'});
  // });

  jQuery('.main-navigation-link.parent-menu').hover(function () {
    jQuery(this).next().css({'display': 'block'});
  });

  jQuery('.navbar-list-item.menuparent').on('click','.navbar-icon', function(event){
    event.stopPropagation();
    if (jQuery(this).prev().css('display') === 'block') {
      jQuery(this).prev().css({'display': 'none'});
      jQuery(this).prev().removeClass('dropdown-collapsed');
      jQuery(this).prev().prev().removeClass('child-colapse');
    } else {
      jQuery(this).prev().css({'display': 'block'});
      jQuery(this).prev().addClass('dropdown-collapsed');
      jQuery(this).prev().prev().addClass('child-colapse');
    }
  });

  jQuery('.slider-productos-destacados').next().remove();
  jQuery('.navbar-list-item.menuparent').append('<i class="navbar-icon fa fa-caret-down"></i>');
  jQuery( "<h2 class='contact-form-title'>¡Queremos escucharte!</h2>" ).insertBefore( jQuery( ".feedback--text-container p" ) );
  jQuery( "<div class='contact-form-number-container'>" +
      "<p class='contact-form-number-info'>Para verificar sus puntos y/o consultas de servicios, llámenos a la Línea de Atención</p>" +
      "<label class='contact-form-number'><a href='tel:2705535'>270-5535</a></label>" +
      "</div>" ).insertBefore( jQuery( ".feedback--text-container p" ) );
});
