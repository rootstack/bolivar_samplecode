jQuery(document).ready(function(jQuery){
	var mainHeader = jQuery('.menu--header'),
		secondaryNavigation = jQuery('.cd-secondary-nav'),
		//this applies only if secondary nav is below intro section
		belowNavHeroContent = jQuery('.sub-nav-hero'),

		headerHeight = mainHeader.height();

	//set scrolling variables
	var scrolling = false,
		previousTop = 0,
		currentTop = 0,
		scrollDelta = 10,
		scrollOffset = 150,
		contentHide = false;

	var toolbarAdmin = jQuery('#toolbar-administration').html();

	if (typeof toolbarAdmin != "undefined") {
		jQuery('.menu--header').addClass('header-top-fix');
	}

	mainHeader.on('click', '.nav-trigger', function(event){
		// open primary navigation on mobile
		event.preventDefault();
		jQuery('.cd-navigation').height(jQuery(window).height() - 50);
		var socialMenu = jQuery('.social-menu.menu.nav').html();
		var searchField = jQuery('.region-hero .hero-overlay').html();
		jQuery('.menu--header .social--menu').html(socialMenu);
		jQuery('.mobile-search-field').html(searchField);
		mainHeader.toggleClass('nav-open');
		if (contentHide) {
		  contentHide = false;
		  jQuery('.hero').show();
		  jQuery('.featured--blocks-home').show();
		  jQuery('.main-container').show();
		  jQuery('.footer').show();
		  jQuery('header.navbar.navbar-default').show();
		  jQuery('.breadcrumbs').show();
		  jQuery('.menu--header img.logo').hide();
		} else {
		  contentHide = true;
		  jQuery('.hero').hide();
		  jQuery('.featured--blocks-home').hide();
		  jQuery('.main-container').hide();
		  jQuery('.footer').hide();
		  jQuery('header.navbar.navbar-default').hide();
		  jQuery('.breadcrumbs').hide();
		  jQuery('.menu--header img.logo').show();
		}
	});

	jQuery(window).on('scroll', function(){
		// if( !scrolling ) {
		// 	scrolling = true;
		// 	(!window.requestAnimationFrame)
		// 		? setTimeout(autoHideHeader, 250)
		// 		: requestAnimationFrame(autoHideHeader);
		// }
	});

	jQuery(window).on('resize', function(){
		// headerHeight = mainHeader.height();
	});

	function autoHideHeader() {
		var currentTop = jQuery(window).scrollTop();

		( belowNavHeroContent.length > 0 ) 
			? checkStickyNavigation(currentTop) // secondary navigation below intro
			: checkSimpleNavigation(currentTop);

	   	previousTop = currentTop;
		scrolling = false;
	}

	function checkSimpleNavigation(currentTop) {
		//there's no secondary nav or secondary nav is below primary nav
	    if (previousTop - currentTop > scrollDelta) {
	    	//if scrolling up...
	    	mainHeader.removeClass('is-hidden');
	    } else if( currentTop - previousTop > scrollDelta && currentTop > scrollOffset) {
	    	//if scrolling down...
	    	mainHeader.addClass('is-hidden');
	    }
	}

	function checkStickyNavigation(currentTop) {
		//secondary nav below intro section - sticky secondary nav
		var secondaryNavOffsetTop = belowNavHeroContent.offset().top - secondaryNavigation.height() - mainHeader.height();
		
		if (previousTop >= currentTop ) {
	    	//if scrolling up... 
	    	if( currentTop < secondaryNavOffsetTop ) {
	    		//secondary nav is not fixed
	    		mainHeader.removeClass('is-hidden');
	    		secondaryNavigation.removeClass('fixed slide-up');
	    		belowNavHeroContent.removeClass('secondary-nav-fixed');
	    	} else if( previousTop - currentTop > scrollDelta ) {
	    		//secondary nav is fixed
	    		mainHeader.removeClass('is-hidden');
	    		secondaryNavigation.removeClass('slide-up').addClass('fixed'); 
	    		belowNavHeroContent.addClass('secondary-nav-fixed');
	    	}
	    	
	    } else {
	    	//if scrolling down...	
	 	  	if( currentTop > secondaryNavOffsetTop + scrollOffset ) {
	 	  		//hide primary nav
	    		mainHeader.addClass('is-hidden');
	    		secondaryNavigation.addClass('fixed slide-up');
	    		belowNavHeroContent.addClass('secondary-nav-fixed');
	    	} else if( currentTop > secondaryNavOffsetTop ) {
	    		//once the secondary nav is fixed, do not hide primary nav if you haven't scrolled more than scrollOffset 
	    		mainHeader.removeClass('is-hidden');
	    		secondaryNavigation.addClass('fixed').removeClass('slide-up');
	    		belowNavHeroContent.addClass('secondary-nav-fixed');
	    	}

	    }
	}
  jQuery('.footer-home-link').text('');
  jQuery('.rey-logo-home').text('');
  jQuery('.rey-logo-home').append('<img class="footer-home-logo" src="../../../../sites/default/files/inline-images/grupo-rey.png" />');
  jQuery('.align-slider-content-left').css('align-items', 'flex-start');
  jQuery('.align-slider-content-right').css('align-items', 'flex-end');
  jQuery('.align-slider-content-center').css('align-items', 'center');

  jQuery('.home-slider-red').css('background-color', '#ff00002b');
  jQuery('.home-slider-black').css('background-color', '#1514146b');
  jQuery('.home-slider-yellow').css('background-color', '#f4ff4c36');

  jQuery('.instagram-footer-icon').text('');
  jQuery('.facebook-footer-icon').text('');
  jQuery('.youtube-footer-icon').text('');
  jQuery('.twitter-footer-icon').text('');

  jQuery('.field--name-field-boton-url a').addClass('btn btn-danger');
});