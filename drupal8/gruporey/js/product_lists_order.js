jQuery(document).ready(function() {
    var windowHeight = window.innerHeight;
    var windowWidth = getWindowWidth();
    jQuery(".form-item-sort-by").clone().appendTo( ".product--list-filters .product--list-filter-select" );
    jQuery(".product--list-filters .product--list-filter-select #edit-sort-by").attr('id', 'edit-sort-by-cloned');
    jQuery(".product--list-filters .product--list-filter-select label[for=edit-sort-by]").attr('for', 'edit-sort-by-cloned');

    jQuery(".form-item-items-per-page").clone().appendTo( ".product--list-filters .product--list-items-per-page-select" );
    jQuery(".product--list-filters .product--list-items-per-page-select #edit-items-per-page").attr('id', 'edit-items-per-page-cloned');
    jQuery(".product--list-filters .product--list-items-per-page-select label[for=edit-items-per-page]").attr('for', 'edit-items-per-page-cloned');

    jQuery(".form-item-title").clone().appendTo( ".product--list-filters .product--list-filter-select" );
    jQuery(".product--list-filters .product--list-filter-select #edit-title").attr('id', 'edit-title-cloned');
    jQuery(".product--list-filters .product--list-filter-select label[for=edit-title]").attr('for', 'edit-title-cloned');

    jQuery("#edit-sort-by").css("display", "none");
    jQuery("#edit-sort-by > ::after").css("display", "none");
    jQuery("label[for='edit-sort-by']").css("display", "none");

    jQuery("#edit-items-per-page").css("display", "none");
    jQuery("#edit-items-per-page > ::after").css("display", "none");
    jQuery("label[for='edit-items-per-page']").css("display", "none");

    jQuery("#edit-title").css("display", "none");
    jQuery("#edit-title > ::after").css("display", "none");
    jQuery("label[for='edit-title']").css("display", "none");

    jQuery("#edit-submit-catalogo-productos-display").css("display", "none");

    jQuery(document).on("change", ".product--list-filter-select #edit-sort-by-cloned", function() {
        jQuery("#edit-sort-by").val(jQuery('.product--list-filters .product--list-filter-select').find(":selected").val());
        jQuery("#edit-submit-catalogo-productos-display").trigger("click");
    });

    jQuery(document).on("change", ".product--list-items-per-page-select #edit-items-per-page-cloned", function() {
        jQuery("#edit-items-per-page").val(jQuery('.product--list-filters .product--list-items-per-page-select').find(":selected").val());
        jQuery("#edit-submit-catalogo-productos-display").trigger("click");
    });

    jQuery(document).on("keyup", ".product--list-filter-select #edit-title-cloned", function(e) {

      if (e.keyCode == 13) {
        jQuery("#edit-title").val(jQuery('.product--list-filters #edit-title-cloned').val());
        jQuery("#edit-submit-catalogo-productos-display").trigger("click");
      }
    });

    jQuery(document).on("change", "input:checkbox", function(element) {
        jQuery("#edit-submit-catalogo-productos-display").trigger("click");
    });

    jQuery('label[for*=edit-field-product-brand-target-id-]').each(function () {
        var checkbox = jQuery(this).find('input[type=checkbox]');
        if (checkbox[0].checked) {
            jQuery(this).addClass("product--filter-selected");
        } else {
            jQuery(this).removeClass("product--filter-selected");
        }
    });

    jQuery('label[for*=edit-field-product-category-target-id-]').each(function () {
        var checkbox = jQuery(this).find('input[type=checkbox]');
        if (checkbox[0].checked) {
            jQuery(this).addClass("product--filter-selected");
        } else {
            jQuery(this).removeClass("product--filter-selected");
        }
    });

    jQuery( ".product--list-filters" ).insertAfter( ".view-header" );

    // jQuery(".product--list-paginator").append(jQuery(".products-list nav.pager-nav").html());
    // jQuery('.products-list')[0].append(jQuery(".product--list-paginator")[0]);
    jQuery(".products-list nav.pager-nav").empty();

    jQuery(document).on('click', '.btn-filter-toggle', function (event) {
      event.preventDefault();

      if (jQuery('.btn-filter-toggle').attr('action') == 'show') {
        jQuery('.facets--products').removeClass('fadeOutLeft');
        // jQuery('.facets--products').removeClass('fadeOut');
        jQuery('.facets--products').addClass('fadeInLeft');
        jQuery('.facets--products').removeClass('hide-facets');
        jQuery('.facets--products').addClass('show-facets');
        jQuery('.btn-filter-toggle').attr('action', 'hide');
      } else {
        jQuery('.facets--products').removeClass('fadeInLeft');
        // jQuery('.facets--products').removeClass('fadeOut');
        jQuery('.facets--products').addClass('fadeOutLeft');
        setTimeout(function() {
          jQuery('.facets--products').removeClass('show-facets');
          jQuery('.facets--products').addClass('hide-facets');
        }, 550);
        jQuery('.btn-filter-toggle').attr('action', 'show');
      }


    });

    jQuery(window).on("resize", function() {
        var currentHeight = window.innerHeight;
        var currentWidth = getWindowWidth();
        var heightChanged = false;
        var widthChanged = false;
        if (windowHeight != currentHeight) {
            windowHeight = currentHeight;
            heightChanged = true;
        }
        if (windowWidth != currentWidth) {
            windowWidth = currentWidth;
            widthChanged = true;
        }
        if (heightChanged && !widthChanged) {
            var isFacetVisible = jQuery('.facets--products').is(":visible");
            if (isFacetVisible) {
                //If only the inner height changed in mobile due to the
                //browser address bar changing the inner height then do nothing.
                return;
            }
        }

        if (getWindowWidth() > 992) {
          // jQuery('.facets--products').addClass('fadeInLeft');
          // jQuery('.facets--products').removeClass('fadeOut');
          jQuery('.facets--products').addClass('show-facets');
          jQuery('.facets--products').removeClass('hide-facets');
        } else {
          // jQuery('.facets--products').addClass('fadeOut');
          // jQuery('.facets--products').removeClass('fadeInLeft');

          // var isFacetValid = jQuery('.facets--products').is(":visible");
          setTimeout(function() {
            jQuery('.facets--products').removeClass('show-facets');
            jQuery('.facets--products').addClass('hide-facets');
          }, 550);
        }
    });

  function getWindowWidth() {
    var windowWidth = 0;
    if (typeof(window.innerWidth) == 'number') {
      windowWidth = window.innerWidth;
    }
    else {
      if (document.documentElement && document.documentElement.clientWidth) {
        windowWidth = document.documentElement.clientWidth;
      }
      else {
        if (document.body && document.body.clientWidth) {
          windowWidth = document.body.clientWidth;
        }
      }
    }
    return windowWidth;
  }

});