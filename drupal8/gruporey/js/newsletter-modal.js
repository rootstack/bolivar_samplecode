/**
 * Created by ivan on 4/20/17.
 */
jQuery(document).ready(function () {
    jQuery(document).bind('cbox_closed', function () {
        jQuery('#colorbox').removeClass("colorbox-modal-form");
    });

    jQuery(document).bind('cbox_complete', function () {
        jQuery('#colorbox').resize();
    });

    jQuery(document).on('click', 'a.newsletter-modal-link', function (e) {
        //colorbox coloca su propio listener click. Adicionar un class 'colorbox-initialized'
        //y en el proximo click event revisar si esta clase existe
        if (!jQuery(this).hasClass("colorbox-initialized")) {
            e.preventDefault();
            jQuery(this).colorbox(cboxOptionsNewsletter).addClass("colorbox-initialized");
        }
        if (!jQuery('#colorbox').hasClass("colorbox-modal-form")) {
            jQuery('#colorbox').addClass("colorbox-modal-form");
        }

    });

    jQuery(window).resize(function(){
        startModalResize();
    });

    var cboxOptionsNewsletter = {
        open: true,
        iframe: true,
        maxWidth: '800px',
        maxHeight: '500px',
        minWidth: '320px',
        minHeight: '400px',
        scrolling: false,
        onComplete:function() {
            startModalResize();
        }
    };
    var widthMaxResponisve = 881;
    var nameGrupoReyModal = "grupo_rey_newsletter_modal_prod";

    function getModalWidth() {
        if(window.innerWidth <= widthMaxResponisve) {
            return cboxOptionsNewsletter.minWidth;
        }
        return cboxOptionsNewsletter.maxWidth;
    }

    function getModalHeight() {
        if(window.innerWidth <= widthMaxResponisve) {
            return cboxOptionsNewsletter.minHeight;
        }
        return cboxOptionsNewsletter.maxHeight;
    }

    function startModalResize() {
        jQuery('a.newsletter-modal-link').colorbox.resize({
            width: getModalWidth(),
            height: getModalHeight()
        });
    }

    function startNewsletterModal() {
        if (!retrieve_cookie(nameGrupoReyModal)) {
            var expirationDays = 30;
            var cookie_name = nameGrupoReyModal;
            var cookie_value = 'some-token-value';
            create_cookie(cookie_name, cookie_value, expirationDays, "/");
            if (!window.location.pathname.endsWith('/blog')
                && !window.location.pathname.endsWith('/multipagos')) {
                openNewsletterModal();
            }
        }
    }

    function openNewsletterModal() {
        jQuery('.newsletter-modal-link').trigger('click');
    }

    function create_cookie(name, value, days2expire, path) {
        var date = new Date();
        date.setTime(date.getTime() + (days2expire * 24 * 60 * 60 * 1000));
        var expires = date.toUTCString();
        document.cookie = name + '=' + value + ';' +
            'expires=' + expires + ';' +
            'path=' + path + ';';
    }

    function retrieve_cookie(name) {
        var cookie_value = "",
            current_cookie = "",
            name_expr = name + "=",
            all_cookies = document.cookie.split(';'),
            n = all_cookies.length;
        for(var i = 0; i < n; i++) {
            current_cookie = all_cookies[i].trim();
            if(current_cookie.indexOf(name_expr) == 0) {
                cookie_value = current_cookie.substring(name_expr.length, current_cookie.length);
                break;
            }
        }
        return cookie_value;
    }

    startNewsletterModal();
});



