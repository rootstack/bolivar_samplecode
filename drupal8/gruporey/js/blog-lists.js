jQuery(document).ready(function() {
  jQuery( "nav.pager-nav.text-center" ).insertAfter( ".view-header" );
  jQuery( "nav.pager-nav.text-center" ).removeClass("text-center");
  jQuery( "nav.pager-nav" ).addClass("pull-right blog--list-paginator");
  //Move view elements from view to an element with full width
  jQuery('.view-grupo-rey-blog-list .view-header').appendTo('.block--bef');
  jQuery('.view-grupo-rey-blog-list .blog--list-paginator').appendTo('.block--bef');
  jQuery('.view-grupo-rey-blog-list .view-filters.form-group').appendTo('.block--bef');
});

jQuery(".bef-links .form-group a").each(function (index, value){
  var currentUrl = jQuery(this).attr('href');
  var newUrl = currentUrl.replace('blog-old', 'blog');
  jQuery(this).attr('href', newUrl);
});
