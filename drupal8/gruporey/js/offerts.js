jQuery(document).ready(function() {

  var pathname = window.location.pathname;
  var url      = window.location.href;

  if (pathname != '/') {
    url = url.replace(pathname, '') + "/multipagos";
  }  else {
    url = url + "multipagos";
  }

  console.log(url);

  jQuery('a.main-navigation-link a.multipagos--modal').colorbox({
    iframe:true,
    width:"45%",
    href: url,
    height: "70%"
  });

  jQuery(document).on('click', 'a.main-navigation-link a.multipagos--modal', function(e) {
    console.log("inside click for multipago");
    jQuery('#colorbox').addClass('modal--multipagos');
    e.preventDefault();
  });

  jQuery(document).bind('cbox_closed', function () {
    console.log('closing multipagos');
    jQuery('#colorbox').removeClass('modal--multipagos');
  });

  jQuery('.form-checkbox').after(" ");

});