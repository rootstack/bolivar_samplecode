jQuery(document).ready(function() {

  var pathname = window.location.pathname;
  var url      = window.location.href;

  if (pathname != '/') {
    // url = url.replace(pathname, '') + "/multipagos";
    url = '/multipagos';
  }  else {
    url = '/multipagos';
  }

  jQuery('.multipagos--modal').colorbox({
    iframe:true,
    width:"45%",
    href: url,
    height: "70%"
  });

  jQuery(document).on('click', '.multipagos--modal', function(e) {
    jQuery('#colorbox').addClass('modal--multipagos');
    e.preventDefault();
  });

  jQuery(document).bind('cbox_closed', function () {
    jQuery('#colorbox').removeClass('modal--multipagos');
  });

});