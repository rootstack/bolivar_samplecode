jQuery(document).ready(function(){
  jQuery(document).on('click', 'li button.top-bar-busqueda', handleBusquedaClick);

});
var active = false;
function handleBusquedaClick() {
  active = !active;
  if (active) {
    jQuery(".hero-overlay").show();
  } else {
    jQuery(".hero-overlay").hide();
  }

  jQuery(".flex-prev").addClass('fa fa-home ');
}