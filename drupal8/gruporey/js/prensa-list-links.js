/**
 * Created by ivan on 5/8/17.
 */
jQuery(document).ready(function() {
  jQuery( "nav.pager-nav.text-center" ).removeClass("text-center");
  jQuery('.view-centro-de-prensa .view-header').appendTo('.block--bef');
  jQuery('.view-centro-de-prensa .view-filters.form-group').appendTo('.block--bef');
  jQuery('.view-centro-de-prensa .blog--list-paginator').appendTo('.block--bef');
});

jQuery(".bef-links .form-group a").each(function (index, value){
  var currentUrl = jQuery(this).attr('href');
  var newUrl = currentUrl.replace('centro-prensa', 'prensa');
  jQuery(this).attr('href', newUrl);
});