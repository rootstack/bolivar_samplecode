var gulp = require('gulp');

// var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');
var browserSync = require('browser-sync');
var reload = browserSync.reload;
// var htmlmin = require('gulp-htmlmin');

// gulp.task('lint', function() {
//   return gulp.src('js/*.js')
//     .pipe(jshint())
//     .pipe(jshint.reporter('default'));
// });

gulp.task('sass', function() {
  return gulp.src('scss/styles.scss')
    .pipe(sass())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('css/'));
});

gulp.task('print', function() {
  return gulp.src('scss/print.scss')
    .pipe(sass())
    .pipe(rename('print.css'))     
    .pipe(gulp.dest('css/'));
});

// BrowserSync
gulp.task('browser-sync', function() {
  //watch files
  var files = [
    'templates/**/*.twig',
      'js/*.js'
  ];
  //initialize browsersync
  browserSync.init(files, {
    proxy: "rey.rootstack.local", // BrowserSync proxy. Change as needed for your local dev but do not commit these changes.
    port: 8000
  });
});

// gulp.task('scripts', function() {
//   return gulp.src('js/*.js')
//     .pipe(concat('all.js'))
//     .pipe(gulp.dest('dist'))
//     .pipe(rename('all.min.js'))
//     .pipe(uglify())
//     .pipe(gulp.dest('dist/js'));
// });

// gulp.task('minify', function() {
//   return gulp.src('html/*.html')
//     .pipe(htmlmin({collapseWhitespace: true}))
//     .pipe(gulp.dest(''))
// });

gulp.task('watch', function() {
  // gulp.watch('js/*.js', ['lint', 'scripts']);
  gulp.watch(['scss/*.scss', "scss/*/*.scss"], ['sass', 'print']);
});

gulp.task('default', ['sass', 'print', 'watch', 'browser-sync']);